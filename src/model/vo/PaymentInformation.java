package model.vo;

/**
 * Representation of PaymentInformation object.
 */
public class PaymentInformation 
{
	
	/**
	 * Costo total del servicio.
	 */
	private double total;
	
	/**
	 * Propina que dej� el pasajero.
	 */
	private double tips;
	
	/**
	 * Tarifa del viaje.
	 */
	private double fare;
	
	/**
	 * Cobros extras que tuvo el viaje.
	 */
	private double extras;
	
	/**
	 * Tipo de pago.
	 */
	private String paymentType;
	
	/**
	 * Costo de los peajes por los que pas� el servicio.
	 */
	private double tolls;
	
	/**
	 * M�todo constructor del PaymentInformation.
	 */
	public PaymentInformation(double total, double tips, double fare, double extras, 
			String paymentType, double tolls)
	{
		this.total = total;
		this.tips = tips;
		this.fare = fare;
		this.extras = extras;
		this.paymentType = paymentType;
		this.tolls = tolls;
	}
	
	/**
	 * M�todo que retorna el total.
	 */
	public double getTotal()
	{
		return total;
	}
	
	/**
	 * M�todo que retorna la propina.
	 */
	public double getTips()
	{
		return tips;
	}
	
	/**
	 * M�todo que retorna la tarifa.
	 */
	public double getFare()
	{
		return fare;
	}
	
	/**
	 * M�todo que retorna los extras.
	 */
	public double getExtras()
	{
		return extras;
	}
	
	/**
	 * M�todo que retorna el tipo de pago.
	 */
	public String getPaymentType()
	{
		return paymentType;
	}
	
	/**
	 * M�todo que retorna el costo de los peajes.
	 */
	public double getTolls()
	{
		return tolls;
	}
	
}
