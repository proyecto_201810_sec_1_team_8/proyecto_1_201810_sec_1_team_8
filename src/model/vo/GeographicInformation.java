package model.vo;

public class GeographicInformation {
	
	/**
	 * Census track de recogida
	 */
	private int pickupCensusTrack;
	
	/**
	 * latitud de recogida
	 */
	private double pickupCentroidLatitude;
	
	/**longitud de recogida
	 * 
	 */
	private double pickupCentroidLongitude;
	
	/**
	 * coordenadas de recogida
	 */
	private double[] pickupCentroidCoordinates;
	
	/**
	 * tipo de lugar de recogida
	 */
	private String pickupLocationType;
	
	/**
	 * area de recogida
	 */
	private short pickupCommunityArea;
	
	/**
	 * Census track de llegada
	 */
	private int dropoffCensusTrack;
	
	/**
	 * latitud de llegada
	 */
	private double dropoffCentroidLatitude;
	
	/**
	 * longitud de llegada
	 */
	private double dropoffCentroidLongitude;
	
	/**
	 * coordenadas de llegada
	 */
	private double[] dropoffCentroidCoordinates;
	
	/**
	 * tipo de lugar de llegada
	 */
	private String dropoffLocationType;
	
	/**
	 * area de llegada
	 */
	private short dropoffCommunityArea;
	
	/**
	 * millas del viaje
	 */
	private double tripMiles;
	
	/**
	 * M�todo constructor de la informaci�n geogr�fica.
	 */
	public GeographicInformation(int pickupCensusTrack, double pickupCentroidLatitude, double pickupCentroidLongitude,
			double[] pickupCentroidCoordinates, String pickupLocationType, short pickupCommunityArea,
			int dropoffCensusTrack, double dropoffCentroidLatitude,double dropoffCentroidLongitude,double[] dropoffCentroidCoordinates,
			String dropoffLocationType,short dropoffCommunityArea ,double tripMiles)
	{
		this.dropoffCensusTrack=dropoffCensusTrack;
		this.dropoffCentroidCoordinates=dropoffCentroidCoordinates;
		this.dropoffCentroidLatitude=dropoffCentroidLatitude;
		this.dropoffCentroidLongitude=dropoffCentroidLatitude;
		this.dropoffCommunityArea=dropoffCommunityArea;
		this.dropoffLocationType=dropoffLocationType;
		this.pickupCensusTrack=pickupCensusTrack;
		this.pickupCentroidCoordinates=pickupCentroidCoordinates;
		this.pickupCentroidLatitude=dropoffCentroidLatitude;
		this.pickupCentroidLongitude=pickupCentroidLongitude;
		this.pickupCommunityArea=pickupCommunityArea;
		this.pickupLocationType=pickupLocationType;
		this.tripMiles=tripMiles;
	}
	
	// ASUMIR QUE ESOS RETURN EST�N HECHOS PORQUE NADA QUE VER.
	
}
