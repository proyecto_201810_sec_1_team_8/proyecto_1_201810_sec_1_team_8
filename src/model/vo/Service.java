package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service>
{	
	
	/**
	 * Id del servicio.
	 */
	private String tripId;
	
	/**
	 * Taxi en que se tom� el servicio.
	 */
	private String taxi_id;
	
	/**
	 * Informaci�n relacionada con el pago.
	 */
	private PaymentInformation paymentInformation;
	
	/**
	 * Informaci�n relacionada el espacio.
	 */
	private GeographicInformation geographicInformation;
	
	/**
	 * Momento en que empez� el viaje.
	 */
	private long startTime;
	
	/**
	 * Momento en que termin� el viaje.
	 */
	private long endTime;
	
	/**
	 * M�todo constructor del servicio.
	 */
	public Service(String tripId, String taxi_id, PaymentInformation paymentInformation, 
			GeographicInformation geographicInformation, long startTime, long endTime)
	{
		this.tripId = tripId;
		this.taxi_id = taxi_id;
		this.paymentInformation = paymentInformation;
		this.geographicInformation = geographicInformation;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	/**
	 * M�todo que retorna el tripId.
	 */
	public String getTripId()
	{
		return tripId;
	}
	
	/**
	 * M�todo que retorna el taxi.
	 */
	public String getTaxi_Id()
	{
		return taxi_id;
	}
	
	/**
	 * M�todo que retorna la informaci�n relacionada con el pago.
	 */
	public PaymentInformation getPaymentInformation()
	{
		return paymentInformation;
	}
	
	/**
	 * M�todo que retorna la informaci�n relacionada con el espacio.
	 */
	public GeographicInformation getGeographicInformation()
	{
		return geographicInformation;
	}
	
	/**
	 * M�todo que retorna el tiempo de inicio del servicio.
	 */
	public long getStartTime()
	{
		return startTime;
	}
	
	/**
	 * M�todo que retorna el tiempo de terminaci�n del servicio.
	 */
	public long getEndTime()
	{
		return endTime;
	}
	
	/**
	 * M�todo que retorna la duraci�n del servicio.
	 */
	public long getTimeDuration()
	{
		return endTime - startTime;
	}
	
	/**
	 * M�todo encargado de comparar this servicio con otro.
	 */
	public int compareTo(Service service)
	{
		int comp = tripId.compareTo(service.getTripId());
		
		if(comp < 0)
		{
			return -1;
		}
		else if(comp == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public String toString(){
		return tripId;
	}
	
	
}
