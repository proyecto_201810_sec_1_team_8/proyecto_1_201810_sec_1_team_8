package model.vo;

import java.util.LinkedList;
import java.util.List;
// TODO: Ligar los nuestros.

public class CommunityArea implements Comparable<CommunityArea>
{

	/**
	 * Codigo de la zona de la ciudad
	 */
	private short code;
	
	/**
	 * Lista de servicios que iniciaron en la zona
	 */
	private List<Service> startServices;
	
	/**
	 * Lista de servicios que iniciaron y terminaron en la zona
	 */
	private List<Service> startEndServices;
	
	/**
	 * Lista de servicios que terminaron en la zona
	 */
	private List<Service> endServices;
	
	/**
	 * Constructor de la zona de la ciudad
	 * @param code
	 */
	public CommunityArea(short code)
	{
		this.code = code;
		startServices = new LinkedList<Service>();
		startEndServices = new LinkedList<Service>();
		endServices = new LinkedList<Service>();
	}
	
	/**
	 * Retorna el codigo de la zona de la ciudad
	 * @return codigo de la zona
	 */
	public short getCode()
	{
		return code;
	}
	
	/**
	 * Da lista de servicios que inician en la zona 
	 * @param beginingDate
	 * @param endDate
	 * @return lista de servicios que inician en la zona
	 */
	public List<Service> getStartServices(long beginingDate, long endDate)
	{
		return startServices;
	}
	
	/**
	 * Da lista de servicios que inician y terminan en la zona 
	 * @param beginingDate
	 * @param endDate
	 * @return lista de servicios que inician y terminan en la zona
	 */
	public List<Service> getStartEndServices(long beginingDate, long endDate)
	{
		return startEndServices;
	}
	
	/**
	 * Da lista de servicios que inician en la zona 
	 * @param beginingDate
	 * @param endDate
	 * @return lista de servicios que terminan en la zona
	 */
	public List<Service> getEndServices(long beginingDate, long endDate)
	{
		return endServices;
	}
	
	/**
	 * M�todo que le compara con otras �reas.
	 */
	public int compareTo(CommunityArea communityArea)
	{
		if(code < communityArea.getCode())
		{
			return -1;
		}
		else if(code == communityArea.getCode())
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
/**
 * 
 * @param service
 * @param start_0___end_1____start_end_2
 */
	public void addService(Service service, int start_0___end_1____start_end_2) {
		// TODO Auto-generated method stub
		if(start_0___end_1____start_end_2==0)startServices.add(service);
		if(start_0___end_1____start_end_2==1)endServices.add(service);
		if(start_0___end_1____start_end_2==2)startEndServices.add(service);
		
	}
	
}
