package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.ListaEncadenada;

//TODO: Cambiar la implementaci�n de Java por la nuestra.

public class Company implements Comparable<Company> 
{

	/**
	 * Lista de taxis de la compa��a	
	 */
	private ListaEncadenada<Taxi> taxiList;

	/**
	 * Nombre de la compa��a.
	 */
	private String companyName;

	/**
	 * M�todo constructor de la compa��a.
	 */
	public Company(String companyName)
	{
		this.companyName = companyName;
		taxiList = new ListaEncadenada<Taxi>();
	}
	
	/**
	 * M�todo que retorna el nombre de la compa��a.
	 */
	public String getCompanyName()
	{
		return companyName;
	}
	
	/**
	 * M�todo que retorna la lista de taxis.
	 */
	public ListaEncadenada<Taxi> getTaxiList()
	{
		return taxiList;
	}
	
	/**
	 * M�todo que retorna el taxi con m�s cantidad de servicios.
	 */
	public Taxi getMostServicesTaxi(long startDate, long endDate)
	{
		return null; 
	}

	/**
	 * M�todo que compara la compa��a con otra.
	 */
	public int compareTo(Company company)
	{
		int comp = companyName.compareTo(company.getCompanyName());

		if(comp < 0)
		{
			return -1;
		}
		else if(comp == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	/**
	 * M�todo que agrega un nuevo taxi a la compa��a.
	 */
	public Taxi addTaxi(Taxi taxi)
	{
		//TODO BUSCA Y RETORNA EL TAXI
		return taxiList.add(taxi);
	}
	public String toString(){
		if(companyName.equalsIgnoreCase(""))
			return "Taxis sin companias "+taxiList.size()+" taxis";
		return companyName+" con "+taxiList.size()+" taxis";
	}

}
