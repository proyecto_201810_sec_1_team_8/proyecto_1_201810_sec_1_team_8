package model.vo;

import java.util.LinkedList;
import java.util.List;

// TODO: Cambiar la implementaci�n de Java por la nuestra.

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	
	/**
	 * Id del taxi.
	 */
	private String taxiId;
	
	/**
	 * Lista de servicios del taxi;
	 */
	private List<Service> servicesList;
	
	/**
	 * Compa��a a la que est� afiliado el taxi. Null en caso de no tener.
	 */
	private String company;
	
	/**
	 * M�todo constructor del taxi.
	 */
	public Taxi(String taxiId, String company)
	{
		this.taxiId = taxiId;
		this.company = company;
		servicesList = new LinkedList<Service>();
	}
	
	/**
	 * M�todo que retorna el id del taxi.
	 */
	public String getTaxiId()
	{
		return taxiId;
	}
	
	/**
	 * M�todo que retorna la lista de servicios.
	 */
	public List<Service> getServicesList()
	{
		return servicesList;
	}
	
	/**
	 * M�todo que retorna la compa��a, null en caso de no tener.
	 */
	public String getCompany()
	{
		return company;
	}
	
	/**
	 * M�todo que compara este taxi con otro.
	 */
	public int compareTo(Taxi taxi)
	{
		int comp = taxiId.compareTo(taxi.getTaxiId());

		if(comp < 0)
		{
			return -1;
		}
		else if(comp == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	/**
	 * M�todo que agrega un servicio.
	 */
	public void addService(Service service)
	{
		servicesList.add(service);
	}
	
	
	public String toString(){
		return taxiId+" con "+servicesList.size()+" servicios";
	}
}