package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Date;
import java.util.LinkedList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import model.vo.Company;
import model.vo.GeographicInformation;
import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.ListaEncadenada;
import model.vo.CommunityArea;
import model.vo.Company;
import model.vo.PaymentInformation;
import model.vo.Service;
import model.vo.Taxi;

public class TaxiTripsManager implements ITaxiTripsManager
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	/**
	 * Lista de servicios
	 */
	private ListaEncadenada<Service> services;

	/**
	 * Lista de zonas de la ciudad
	 */
	private ListaEncadenada<CommunityArea> communityAreas;

	/**
	 * Lista de companias
	 */
	private ListaEncadenada<Company> companies;

	/**
	 * Lista de taxis
	 */
	private ListaEncadenada<Taxi> taxiList;

	public boolean cargarSistema(String direccionJson) 
	{
		System.out.println("Inside loadServices with " + direccionJson);
		//Inicializacion de las listas
		services = new ListaEncadenada<Service>();
		taxiList = new ListaEncadenada<Taxi>();
		companies = new ListaEncadenada<Company>();
		communityAreas=new ListaEncadenada<CommunityArea>();
		JsonParser parser = new JsonParser();
		//Creacion de los elementos que posteriormente se anadiran a las respectivas listas
		Service s;
		Taxi t;
		Company company;
		PaymentInformation paymentInformation;
		GeographicInformation geographicInformaton;
		CommunityArea communityArea;



		try {
			JsonArray array= (JsonArray) parser.parse(new FileReader(direccionJson));
			for (int i = 0; i < array.size(); i++) {
				JsonObject j= (JsonObject) array.get(i);
				cargarInformacion(j);
				System.out.println(i);
			}
			System.out.println(array.size());


		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		services.pack();
		communityAreas.pack();
		companies.pack();
		taxiList.pack();
		System.out.println("---------LISTADO COMPANIAS----------");
		System.out.println(companies.toString());
		System.out.println("---------LISTADO TAXIS----------");
		System.out.println(taxiList.toString());
		System.out.println("---------LISTADO SERVICIOS----------");
		System.out.println(services.size());
		System.out.println("Verificacion pack: Servicios->"+services.getNodes().size()+" Taxis->"+
		taxiList.getNodes().size()+" Companies->"+companies.getNodes().size()+"ComunnityAreas->"+communityAreas.getNodes().size());
		
		
		
		return false;
	}





	/**
	 * Carga la informacion encontrada en cada elemento del JSON
	 * @param object
	 */
	public void cargarInformacion(JsonObject object){
		//******************************
		//Lectura de cada elemento
		//******************************
		
		String company="";
		if (object.get("company")!=null)
			company=object.get("company").getAsString();
		
		int dropoff_census_track=0;
		if (object.get("dropoff_census_track")!=null)
			dropoff_census_track=object.get("dropoff_census_track").getAsInt();

		double dropoff_centroid_latitude=0;
		if (object.get("dropoff_centroid_latitude")!=null)
			dropoff_centroid_latitude=object.get("dropoff_centroid_latitude").getAsDouble();
		
		//CENTROID LOCATION DROPOFF
		String type_dropoff="";
		double[]coordinates_dropoff=new double[2];
		if (object.get("dropoff_census_location")!=null){
			JsonObject ob=object.get("dropoff_census_location").getAsJsonObject();
			JsonElement p=ob.get("coordinates");
			if(p!=null){
				JsonArray arr=p.getAsJsonArray();
				coordinates_dropoff[0]=arr.get(0).getAsDouble();
				coordinates_dropoff[1]=arr.get(1).getAsDouble();
			}
			
			if (ob.get("type")!=null)
				type_dropoff=ob.get("type").getAsString();
		}
		
		double dropoff_centroid_longitude=0;
		if (object.get("dropoff_centroid_longitude")!=null)
			dropoff_centroid_longitude=object.get("dropoff_centroid_longitude").getAsDouble();
		
		short dropoff_community_area=0;
		if (object.get("dropoff_community_area")!=null)
			dropoff_community_area=object.get("dropoff_community_area").getAsShort();
		
		double extras=0;
		if (object.get("extras")!=null)
			extras=object.get("extras").getAsDouble();
		
		double fare=0;
		if (object.get("fare")!=null)
			fare=object.get("fare").getAsDouble();
		
		String payment_type="";
		if (object.get("payment_type")!=null)
			payment_type=object.get("payment_type").getAsString();
		
		int pickup_census_track=0;
		if (object.get("pickup_census_track")!=null)
			pickup_census_track=object.get("pickup_census_track").getAsInt();

		double pickup_centroid_latitude=0;
		if (object.get("pickup_centroid_latitude")!=null)
			pickup_centroid_latitude=object.get("pickup_centroid_latitude").getAsDouble();
		
		//CENTROID LOCATION PICKUP
		String type_pickup="";
		double[]coordinates_pickup=new double[2];
		if (object.get("pickup_census_location")!=null){
			JsonObject ob=object.get("pickup_census_location").getAsJsonObject();
			JsonElement p=ob.get("coordinates");
			if(p!=null){
				JsonArray arr=p.getAsJsonArray();
				coordinates_pickup[0]=arr.get(0).getAsDouble();
				coordinates_pickup[1]=arr.get(1).getAsDouble();
			}
			
			if (ob.get("type")!=null)
				type_pickup=ob.get("type").getAsString();
		}
		
		double pickup_centroid_longitude=0;
		if (object.get("pickup_centroid_longitude")!=null)
			pickup_centroid_longitude=object.get("pickup_centroid_longitude").getAsDouble();
		
		short pickup_community_area=0;
		if (object.get("pickup_community_area")!=null)
			pickup_community_area=object.get("pickup_community_area").getAsShort();
		
		String taxi_id=" ";
		if (object.get("taxi_id")!=null)
			taxi_id=object.get("taxi_id").getAsString();
		
		double tips=0;
		if (object.get("tips")!=null)
			tips=object.get("tips").getAsDouble();
		
		double tolls=0;
		if (object.get("tolls")!=null)
			tolls=object.get("tolls").getAsDouble();
		
		long trip_end_timestamp=0;
		if (object.get("trip_end_timestamp")!=null)
			trip_end_timestamp=traducirFecha(object.get("trip_end_timestamp").getAsString());
		
		String trip_id=" ";
		if (object.get("trip_id")!=null)
			trip_id=object.get("trip_id").getAsString();
		
		double trip_miles=0;
		if (object.get("trip_miles")!=null)
			trip_miles=object.get("trip_miles").getAsDouble();
		
		int seconds=0;
		if (object.get("trip_seconds")!=null)
			seconds=object.get("trip_seconds").getAsInt();
		
		long trip_start_timestamp=0;
		if (object.get("trip_start_timestamp")!=null)
			trip_start_timestamp=traducirFecha(object.get("trip_start_timestamp").getAsString());
		
		double total_cost= 0;
		if (object.get("trip_total")!=null)
			total_cost=object.get("trip_total").getAsDouble();

		
		
		
		//*****************************
		//INSTANCIACION DE CADA ELEMETO
		//--------MUCHO CUIDADO--------
		//*****************************
		
		//OBJETOS
		PaymentInformation payment_information = new PaymentInformation(total_cost, tips, fare, extras, payment_type, tolls);
		GeographicInformation geographi_information = new GeographicInformation(pickup_census_track, dropoff_centroid_latitude, pickup_centroid_longitude, coordinates_pickup, type_pickup, pickup_community_area, dropoff_census_track, dropoff_centroid_latitude, pickup_centroid_longitude, coordinates_dropoff, type_dropoff, dropoff_community_area, trip_miles);
		Service service=new Service(trip_id,taxi_id, payment_information, geographi_information, trip_start_timestamp, trip_end_timestamp);
		Company companyObject=new Company(company);
		Taxi taxi=new Taxi(taxi_id,company);
		//AREAS CIUDAD
		CommunityArea pickupCommunityArea=new CommunityArea(pickup_community_area);
		CommunityArea dropoffCommunityArea=new CommunityArea(dropoff_community_area);
		if(pickup_community_area==dropoff_community_area){
			//ADD FUNCIONA COMO BUSCAR
			communityAreas.add(pickupCommunityArea).addService(service, 2);
		}else{
			communityAreas.add(pickupCommunityArea).addService(service, 0);
			communityAreas.add(pickupCommunityArea).addService(service, 1);
		}
				
		//*************
		//PARTE DIFICIL
		//*************
		
		//MUYYY IMPORTANTE
		//EL ADD FUNCIONA COMO BUSCAR
		(companies.add(companyObject).addTaxi(taxi)).addService(service);
		
		taxi.addService(service);
		taxiList.add(taxi);
		services.add(service);		
		
	
	}
	
	

	/**
	 * Convierte una fecha a milisegundos desde el 1 de enero de 1970
	 * @param asString
	 * @return long milisegundos 
	 */
	public long traducirFecha(String asString) {
		//Ejemplo fecha
		//2017-02-01T09:00:00.000
		//System.out.println(asString);
		//0T1
		String [] div=asString.split("T");
		//0-1-2
		String [] date=div[0].split("-");
		//0:1:2
		String [] time=div[1].split(":");
		//0.1
		String [] sec=time[2].split("[.]");

		int year=Integer.parseInt(date[0]);
		//System.out.println(year);
		year-=1900;
		int month=Integer.parseInt(date[1]);
		//System.out.println(month);
		month-=1;
		int day=Integer.parseInt(date[2]);
		//System.out.println(day);
		int hour =Integer.parseInt(time[0]);
		//System.out.println(hour+"hora");
		int min=Integer.parseInt(time[1]);
		//System.out.println(min);

		//System.out.println(time[2]);
		int seconds=Integer.parseInt(sec[0]);
		//System.out.println(seconds);

		long msecons=Long.parseLong(sec[1]);
		//System.out.println(msecons);


		Date d=new Date(year, month, day, hour, min, seconds);

		msecons=msecons+d.getTime();

		//d.setTime(msecons);
		//System.out.println(d.toGMTString());

		return msecons;
	}


	
//
//	//1A
//	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//2A
//	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//3A
//	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override //4A
//	public LinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public LinkedList<Compania> darCompaniasTaxisInscritos() 
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//2B
//	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//3B
//	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//4B
//	public LinkedList<Company> darZonasServicios(RangoFechaHora rango)
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//2C
//	public LinkedList<Company> companiasMasServicios(RangoFechaHora rango, int n)
//	{
//		return null;
//	}
//
//	public LinkedList<Company> taxisMasRentables()
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public IStack <Service> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}

}
