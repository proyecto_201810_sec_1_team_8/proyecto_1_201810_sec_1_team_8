package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>> 
{
	/**
	 * add a new element T 
	 * @param element
	 * @return false if element was already on the list
	 */
	public T add(T elemento);
	/**
	 * delete the given element T 
	 * @param element
	 */
	public void delete(T elemento);
	/**
	 * get the given element T (null if it doesn't exist in the list)
	 * @param element
	 * @return element
	 */
	public T get(T element);
	/**
	 * get an element T by position (the first position has the value 0) 
	 * @param position of the element
	 * @return the element en at the position
	 */
	public T get(int posicion);
	/**
	 * return the the number of elements
	 * @return numero de elementos
	 */
	public int size();
	/**
	 * set the listing of elements at the firt element
	 */
	public void listing();
	/**
	 * return the current element T in the listing (return null if it doesn�t exists)
	 * @return element actual in the list, null if doesn't exists
	 */
	public T getCurrent();
	/**
	 * advance to next element in the listing (return if it exists)
	 */
	public T next();
	
}
