package model.data_structures;

public class Node <T extends Comparable<T>>{
	
	private T dato;
	private Node next;
	private Node back;

	public Node(T elemento) {
		dato=elemento;
		next=null;
	}
	public void cambiarSiguiente(Node node){
		next=node;
	}
	public void cambiarAnterior(Node node){
		back=node;
	}
	public Node next(){
		return next;
	}
	public Node back(){
		return back;
	}
	public T get(){
		return dato;
	}

}
