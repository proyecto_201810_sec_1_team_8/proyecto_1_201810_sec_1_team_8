package model.data_structures;

import java.util.ArrayList;

public class ListaEncadenada<T extends Comparable<T>> implements LinkedList<T>{
	
	/**
	 * ArrayList de los nodos
	 */
	private ArrayList<Node>array;
	
	/**
	 * Indica si esta empaquetados los nodos 
	 */
	private boolean packet;
	
	/**
	 * cabeza de la lista
	 */
	private Node first;
	/**
	 * actual de la lista
	 */
	private Node current;
	/**
	 * Tamano de la lista
	 */
	private int size;
	
	/**
	 * Constructor de la lista
	 */
	public ListaEncadenada() {
		// TODO Auto-generated constructor stub
		first=null;
		current=first;
		array=new ArrayList<Node>();
		size=0;
		packet=false;
	}

	@Override
	public T add(T elemento) {
		current=first;
		if(current==null){
			first=new Node<T>(elemento);
			size++;
			packet=false;
			return (T) first.get();
		}
		else{
			if(current.get().compareTo(elemento)==1){
				Node<T> n=new Node<T>(elemento);
				n.cambiarSiguiente(first);
				first.cambiarAnterior(n);
				first=n;
				size++;
				packet=false;
				return (T) first.get();
			}
			else{
				while(current.next()!=null&&current.next().get().compareTo(elemento)<1){
					if(current.next().get().compareTo(elemento)==0)return (T) current.next().get();
					current=current.next();
				}
				if(current.get().compareTo(elemento)==0)
					return (T) current.get();
					
				
				
				Node node=new Node(elemento);
				node.cambiarAnterior(current);
				node.cambiarSiguiente(current.next());
				current.cambiarSiguiente(node.back());
				current.cambiarSiguiente(node);
				size++;
				packet=false;
				return (T) current.next().get();

				
			}
		}
		
		

	}

	@Override
	public void delete(Comparable elemento) {
		// TODO Auto-generated method stub
		Node node=new Node(elemento);
		current=first;
		while(current!=null&&current.get().compareTo(elemento)!=0)
			current=current.next();
		
		if(current==null)
			return;
		else {
			if(current.next()!=null)
				current.next().cambiarAnterior(current.back());
			current.back().cambiarSiguiente(current.next());
			packet=false;
			size--;
		}
		
		
	}

	@Override
	public T get(int posicion) {
		if(packet){
			return (T) array.get(posicion).get();
		}
		else{
			T element=null;
			Node node=first;
			for (int i = 0; i < posicion; i++) {
				node=node.next();
			}
			if(node!=null)
				element=(T) node.get();
			return element;
		}
		
	}
	
	
	public Node getNode(int posicion){
		if(packet)return array.get(posicion);
		else return null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void listing() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return (T) current.get();
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
	
		return (T) current.next().get();
		
	}

	@Override
	public T get(T element) {
		// TODO Auto-generated method stub
		return (T) current.get();
	}
	
	/**
	 * 
	 */
	public void pack(){
		current=first;
		array=new ArrayList<Node>();
		while(current!=null){
			array.add(current);
			current=current.next();
		}
		packet=true;
	}
	
	public boolean isPack(){
		return packet;
	}
	
	
	public ArrayList<Node> getNodes(){
		return array;
	}
	
	public String toString(){
		String resp="Total elementos: "+size;
		current=first;
		while(current!=null){
			resp+="\n"+current.get().toString();
			current=current.next();
		}
		resp+="\n------------------------";
		return resp;
		
	}

}
