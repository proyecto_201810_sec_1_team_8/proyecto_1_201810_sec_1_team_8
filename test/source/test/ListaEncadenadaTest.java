package test;

import junit.framework.TestCase;
import model.data_structures.LinkedList;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase{

	/**
	 * Clase a probar
	 */
	private LinkedList<String> list;
	/**
	 * Escenario a trabajar
	 */
	public void setup(){
		list=new ListaEncadenada<String>();
		list.add("b");
		list.add("e");
		list.add("c");
		list.add("b");
		list.add("g");
		list.add("a");
		list.add("d");
		list.add("b");
		list.add("c");
		list.add("f");
		list.add("a");
		System.out.println(list.toString());
	}
	/**
	 * Prueba 1: verifica eliminar elementos y contar la cantidad de ellos
	 */
	public void test1(){
		setup();
		assertEquals("El numero de elementos debe ser 7",list.size(),7);
		list.delete("r");
		assertEquals("El numero de elementos debe ser 7",list.size(),7);
		list.delete("c");
		assertEquals("El numero de elementos debe ser 6",list.size(),6);
		System.out.println(list.toString());
	}

}
