package test;
import static org.junit.Assert.*;


import junit.framework.TestCase;
import model.data_structures.Node;

public class NodeTest extends TestCase {



	/**
	 * 
	 */
	private Node node;
	/**
	 * 
	 */
	
	private void setupScenario(){
		node=new Node<String>("a");
		
	};
	/**
	 * Prueba 1
	 */

	public void test1(){
		setupScenario();
		assertEquals("El elemento debe ser igual", node.get(), "a");
		assertNull("El siguiente debe ser nulo",node.next());
		assertNull("El anterior debe ser nulo",node.back());
	}
	public void test2(){
		setupScenario();
		node.cambiarAnterior(new Node("b"));
		node.cambiarSiguiente(new Node("c"));
		assertEquals("El elemento debe ser igual", node.get(), "a");
		assertNotNull("El siguiente no debe ser nulo",node.next());
		assertNotNull("El anterior no debe ser nulo",node.back());
	}

	
}